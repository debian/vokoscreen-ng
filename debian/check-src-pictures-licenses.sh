#!/bin/bash

# by Eriberto
# This script will help to check licensing of src/pictures/*

DIRCHECK=$(pwd | egrep '/debian$')

[ "$DIRCHECK" ] || { echo "ERROR: You need execute it in debian/ place."; exit 1; }

find ../src/pictures/ -name '*.license' -printf "%f: " -exec grep license= {} \; \
    | sed 's/license=//' | grep -v template.license | sed 's/^/\n/' | pager
